--- Debian version type and operators                                -*- sql -*-
---
--- Copyright © 2011 Roger Leigh <rleigh@debian.org>
---
--- This program is free software: you can redistribute it and/or modify
--- it under the terms of the GNU General Public License as published by
--- the Free Software Foundation, either version 2 of the License, or
--- (at your option) any later version.
---
--- This program is distributed in the hope that it will be useful, but
--- WITHOUT ANY WARRANTY; without even the implied warranty of
--- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
--- General Public License for more details.
---
--- You should have received a copy of the GNU General Public License
--- along with this program.  If not, see
--- <http://www.gnu.org/licenses/>.


ALTER EXTENSION debversion ADD TYPE debversion;

ALTER EXTENSION debversion ADD FUNCTION debversionin(cstring);
ALTER EXTENSION debversion ADD FUNCTION debversionout(debversion);
ALTER EXTENSION debversion ADD FUNCTION debversionrecv(internal);
ALTER EXTENSION debversion ADD FUNCTION debversionsend(debversion);

ALTER EXTENSION debversion ADD FUNCTION debversion_larger(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_smaller(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_hash(debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_cmp(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_ge(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_gt(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_le(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_lt(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_ne(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion_eq(debversion, debversion);
ALTER EXTENSION debversion ADD FUNCTION debversion(bpchar);

ALTER EXTENSION debversion ADD CAST (bpchar AS debversion);
ALTER EXTENSION debversion ADD CAST (varchar AS debversion);
ALTER EXTENSION debversion ADD CAST (text AS debversion);
ALTER EXTENSION debversion ADD CAST (debversion AS bpchar);
ALTER EXTENSION debversion ADD CAST (debversion AS varchar);
ALTER EXTENSION debversion ADD CAST (debversion AS text);

ALTER EXTENSION debversion ADD OPERATOR > (debversion, debversion);
ALTER EXTENSION debversion ADD OPERATOR >= (debversion, debversion);
ALTER EXTENSION debversion ADD OPERATOR <= (debversion, debversion);
ALTER EXTENSION debversion ADD OPERATOR < (debversion, debversion);
ALTER EXTENSION debversion ADD OPERATOR <> (debversion, debversion);
ALTER EXTENSION debversion ADD OPERATOR = (debversion, debversion);

ALTER EXTENSION debversion ADD AGGREGATE max(debversion);
ALTER EXTENSION debversion ADD AGGREGATE min(debversion);

ALTER EXTENSION debversion ADD OPERATOR CLASS debversion_ops USING hash;
ALTER EXTENSION debversion ADD OPERATOR CLASS debversion_ops USING btree;
