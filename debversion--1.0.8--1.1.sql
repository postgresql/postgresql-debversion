UPDATE pg_operator SET oprrest = 'eqsel'::regproc, oprjoin = 'eqjoinsel'::regproc
  WHERE oprname = '=' AND oprleft = 'debversion'::regtype AND oprright = 'debversion'::regtype;
UPDATE pg_operator SET oprrest = 'neqsel'::regproc, oprjoin = 'neqjoinsel'::regproc
  WHERE oprname = '<>' AND oprleft = 'debversion'::regtype AND oprright = 'debversion'::regtype;
UPDATE pg_operator SET oprrest = 'scalarltsel'::regproc, oprjoin = 'scalarltjoinsel'::regproc
  WHERE oprname IN ('<', '<=') AND oprleft = 'debversion'::regtype AND oprright = 'debversion'::regtype;
UPDATE pg_operator SET oprrest = 'scalargtsel'::regproc, oprjoin = 'scalargtjoinsel'::regproc
  WHERE oprname IN ('>', '>=') AND oprleft = 'debversion'::regtype AND oprright = 'debversion'::regtype;

-- PG 9.6+ has ALTER OPERATOR SET RESTRICT | JOIN ...
