Source: postgresql-debversion
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Michael Banck <mbanck@debian.org>,
 Christoph Berg <myon@debian.org>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 libapt-pkg-dev,
 postgresql-all <!nocheck>,
 postgresql-server-dev-all,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://salsa.debian.org/postgresql/postgresql-debversion
Vcs-Browser: https://salsa.debian.org/postgresql/postgresql-debversion
Vcs-Git: https://salsa.debian.org/postgresql/postgresql-debversion.git

Package: postgresql-17-debversion
Architecture: any
Depends:
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: Debian version number type for PostgreSQL
 Debian version numbers, used to version Debian binary and source
 packages, have a defined format, including specifications for how
 versions should be compared in order to sort them.  This package
 implements a "debversion" type to represent Debian version numbers
 within the PostgreSQL database.  This also includes operators for
 version comparison and index operator classes for creating indexes on
 the debversion type.
 .
 Version comparison uses the algorithm used by the Debian package
 manager, dpkg, using the implementation from libapt-pkg.  This means
 that columns in tables using the debversion type may be sorted and
 compared correctly using the same logic as "dpkg --compare-versions".
 It is also possible to create indexes on these columns.
 .
 postgresql-debversion implements the following features:
 .
  * The "debversion" type (internally derived from the "text" type)
  * A full set of operators for version comparison (< <= = <> >= >)
    including commutator and negator optimisation hints
  * Operator classes for btree and hash indexes
  * The aggregate functions min() and max()
