-- negator for > was wrongly set to >= before, fix to <=
update pg_operator set oprnegate = '<=(debversion,debversion)'::regoperator where oid = '>(debversion,debversion)'::regoperator;
