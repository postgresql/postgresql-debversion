postgresql-debversion
=====================

Debian version number type for PostgreSQL


About
-----

Debian version numbers, used to version Debian binary and source
packages, have a defined format, including specifications for how
versions should be compared in order to sort them.  This package
implements a `debversion` type to represent Debian version numbers
within the PostgreSQL database.  This also includes operators for
version comparison and index operator classes for creating indexes on
the debversion type.

Version comparison uses the algorithm used by the Debian package
manager, dpkg, using the implementation from libapt-pkg.  This means
that columns in tables using the debversion type may be sorted and
compared correctly using the same logic as `dpkg --compare-versions`.
It is also possible to create indexes on these columns.

postgresql-debversion implements the following features:

* The `debversion` type (internally derived from the `text` type)
* A full set of operators for version comparison (< <= = <> >= >)
  including commutator and negator optimisation hints
* Operator classes for btree and hash indexes
* The aggregate functions `min()` and `max()`

Installation
------------

The `libapt-pkg` and PostgreSQL server development headers and
libraries are required. Run:

    make
    make install

or build the Debian packages using

    dpkg-buildpackage -us -uc

If using PostgreSQL 9.0, which do not have support for `CREATE
EXTENSION`, please configure with `--disable-extension` to use the old
style config directory.  Note that this release drops support for
PostgreSQL 8.4 and earlier, which have broken headers (they don't
allow inclusion in C++ code).


Using
-----

Once built and installed, you can install the debversion type into
your database by running the following from the psql shell (PostgreSQL
9.1 and later):

    CREATE EXTENSION debversion;

If you're upgrading from an earlier version without extension support,
then please run

    CREATE EXTENSION debversion FROM unpackaged;

which will add all the existing types to the `debversion` extension.
If you wish to remove it,

    DROP EXTENSION debversion;

will remove `debversion` from the database;

Copyright and License
---------------------

Copyright © 2004-2012 Roger Leigh <rleigh@debian.org>

`debversion` is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

`debversion` is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/].

